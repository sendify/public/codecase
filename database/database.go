package database

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// Config is the required properties to use the database.
type Config struct {
	User     string
	Password string
	Host     string
	Name     string
}

// GetConnection returns a goroutine-safe singleton of the database.
func GetConnection(config Config) (*gorm.DB, error) {
	conn, err := gorm.Open(mysql.New(mysql.Config{
		DSN: fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4&parseTime=True", config.User, config.Password, config.Host, config.Name),
	}))
	if err != nil {
		return nil, fmt.Errorf("failed to run the server: %w", err)
	}

	// auto migrate
	err = migrate(conn)
	if err != nil {
		return nil, err
	}

	return conn, nil
}
