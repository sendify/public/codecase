package shipments_test

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"sendify/codecase/api"
	"sendify/codecase/config"
	"sendify/codecase/database"
	"sendify/codecase/foundation/web"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	gormmysql "gorm.io/driver/mysql"
	"gorm.io/gorm"

	"github.com/DATA-DOG/go-sqlmock"
	_ "github.com/go-sql-driver/mysql"
)

// createDBMock mocks our database. It should only be called for unit tests, not integration tests.
func createDBMock(t *testing.T) (sqlmock.Sqlmock, *gorm.DB) {
	var gdb *gorm.DB
	var mock sqlmock.Sqlmock

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Got error when creating databse mock: %v", err)
	}
	dialector := gormmysql.New(gormmysql.Config{
		Conn:                      db,
		SkipInitializeWithVersion: true,
	})

	gdb, err = gorm.Open(dialector, &gorm.Config{})
	if err != nil {
		t.Errorf("Failed to open Gorm: %v", err)
	}

	if gdb == nil {
		t.Error("Failed to initialize Gorm")
	}
	t.Cleanup(func() {
		db.Close()
	})
	return mock, gdb
}

// makeGetShipmentRequest creates a context for a mocked GET request. This can be used when calling the handler.
func makeGetShipmentRequest(t *testing.T, id uint) (*gin.Context, *httptest.ResponseRecorder) {
	t.Helper()

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)

	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("v2/addressBookSearch/%d", id), nil)
	require.NoError(t, err)
	c.Request = req

	return c, w
}

// TestGetShipment is a unit test suite of the GetShipment function in the shipment handler. It tests both
// happy and sad paths, and therefore mocks the database to speed things up.
func TestGetShipment(t *testing.T) {
	var mock sqlmock.Sqlmock
	var db *gorm.DB
	// Fetch an existing shipment.
	t.Run("non-empty", func(t *testing.T) {
		mock, db = createDBMock(t)
		rest := api.NewAPI(db)

		id := uint(1)
		shipment := database.Shipment{
			FromName:        "Test Name",
			FromAddress:     "Townstreet 23 12222 SomeTown",
			FromCountryCode: "SE",
			ToName:          "Other Name",
			ToAddress:       "4343 Road, City 322AZ",
			ToCountryCode:   "DE",
			WeightKg:        3.50,
			Price:           150,
		}

		mock.ExpectQuery(`.+`). // We only make one database call for this, so we can skip the exact query.
					WillReturnRows(sqlmock.NewRows([]string{"id", "from_name", "from_address", "from_country_code", "to_name", "to_address", "to_country_code", "weight_kg", "price"}).
						AddRow(id, shipment.FromName, shipment.FromAddress, shipment.FromCountryCode, shipment.ToName, shipment.ToAddress, shipment.ToCountryCode, shipment.WeightKg, shipment.Price))

		c, w := makeGetShipmentRequest(t, id)
		rest.Shipments.GetShipment(c)

		require.Equal(t, http.StatusOK, w.Code)
		var response database.Shipment
		err := json.NewDecoder(w.Body).Decode(&response)
		assert.NoError(t, err)
		expected := shipment
		expected.ID = 1 // The returned shipment should be identical as the one mocked in the DB, but we haven't included the ID yet.
		require.Equal(t, expected, response)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	// No shipment with that ID exists in the database
	t.Run("empty", func(t *testing.T) {
		mock, db = createDBMock(t)
		rest := api.NewAPI(db)
		mock.ExpectQuery(`.+`).WillReturnRows(sqlmock.NewRows(nil))

		c, w := makeGetShipmentRequest(t, 1)
		rest.Shipments.GetShipment(c)

		assert.Equal(t, http.StatusNotFound, w.Code)
	})

	// The database call fails
	t.Run("database error", func(t *testing.T) {
		mock, db = createDBMock(t)
		rest := api.NewAPI(db)
		mock.ExpectQuery(`.+`).WillReturnError(errors.New("db error"))

		c, w := makeGetShipmentRequest(t, 1)
		rest.Shipments.GetShipment(c)

		assert.Equal(t, http.StatusInternalServerError, w.Code)
	})
}

// createShipment creates a default test shipment in the database, and returns it.
// The shipment is deleted on cleanup.
func createShipment(t *testing.T, db *gorm.DB) database.Shipment {
	shipment := database.Shipment{
		FromName:        "Anders Liselius",
		FromAddress:     "Nygatan 32 93133 Skellefteå",
		FromCountryCode: "SE",
		ToName:          "Julia Heydecke",
		ToAddress:       "70C Christofstraße Walzbachtal 75045",
		ToCountryCode:   "DE",
		WeightKg:        3.20,
		Price:           150,
	}
	result := db.Create(&shipment)
	if result.Error != nil {
		t.Fatal(result.Error)
	}

	t.Cleanup(func() {
		db.Delete(&shipment)
	})
	return shipment
}

// TestGetShipmentIntegration is an integration test, which tests the functionality from endpoint to database layer.
// This only includes a happy path scenario, since it's fairly slow to run.
//
// The test assumes that a database is running and can be reached.
func TestGetShipmentIntegration(t *testing.T) {
	if testing.Short() {
		// We don't want to run this test when we only run our unit tests.
		t.Skip("skipping integration test")
	}
	cfg, err := config.Load()
	if err != nil {
		log.Fatal(err.Error())
	}
	app, err := web.NewApp(web.Config{
		User:     cfg.Database.User,
		Password: cfg.Database.Password,
		Host:     cfg.Database.Host,
		Name:     cfg.Database.Name,
	})
	if err != nil {
		t.Fatal(err)
	}
	shipment := createShipment(t, app.DB) // Add initial data.
	ts := httptest.NewServer(app.Engine)  // Create test server
	client := &http.Client{}
	defer ts.Close() // Block the server during testing

	ctx := context.Background()
	req, _ := http.NewRequestWithContext(ctx, http.MethodGet, fmt.Sprintf("%s/shipments/%d", ts.URL, shipment.ID), nil)
	resp, err := client.Do(req)

	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	var res database.Shipment
	err = json.NewDecoder(resp.Body).Decode(&res)
	assert.NoError(t, err)

	assert.Equal(t, shipment.ID, res.ID)
	assert.Equal(t, shipment.FromCountryCode, res.FromCountryCode)
	assert.Equal(t, shipment.FromName, res.FromName)
	assert.Equal(t, shipment.ToAddress, res.ToAddress)
	assert.Equal(t, shipment.ToCountryCode, res.ToCountryCode)
	assert.Equal(t, shipment.ToName, res.ToName)
	assert.Equal(t, shipment.WeightKg, res.WeightKg)
	assert.Equal(t, shipment.Price, res.Price)
}
