package shipments

import (
	"errors"
	"net/http"
	"sendify/codecase/database"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type Handler struct {
	DB *gorm.DB
}

// GetShipment returns a single shipment based on its ID.
//
// Returns:
//
//	Status 200 and a shipment (as JSON), if a shipment with the ID exists.
//	Status 404, if no shipment with the ID exists
//	Status 400, if the ID parameter is malformed
//	Status 500, for any internal errors
func (h Handler) GetShipment(ctx *gin.Context) {
	params := IDParams{}
	if err := ctx.ShouldBindUri(&params); err != nil {
		// Something went wrong when parsing the URI. Most likely an ID was not supplied, or was malformed.
		ctx.AbortWithStatusJSON(http.StatusBadRequest, "id is required")
		return
	}

	db := h.DB.WithContext(ctx)
	var shipment database.Shipment
	// Take the first, since the IDs are unique.
	result := db.First(&shipment, params.ID)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			// No shipment with that ID exists. Return a 404.
			ctx.AbortWithStatus(http.StatusNotFound)
			return
		}
		// Something has gone wrong. Abort.
		ctx.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	ctx.JSON(http.StatusOK, shipment)
}
