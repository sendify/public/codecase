package config

import (
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	Database struct {
		User     string `default:"root"`
		Password string `default:""`
		Host     string `default:"sendify_code_case_db:3306"`
		Name     string `default:"sendify"`
	}
	Port string `default:":1234"`
}

// Load returns the application configuration. Values are taken from command line arguments.
func Load() (Config, error) {
	var c Config

	err := envconfig.Process("SENDIFY", &c)
	if err != nil {
		return Config{}, err
	}

	return c, err
}
