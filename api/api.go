package api

import (
	"gorm.io/gorm"
	"sendify/codecase/api/handlers/shipments"
)

type API struct {
	Handlers
}

type Handlers struct {
	Shipments shipments.Handler
}

func NewAPI(db *gorm.DB) API {
	return API{
		Handlers: Handlers{
			Shipments: shipments.Handler{DB: db},
		},
	}
}
