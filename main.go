package main

import (
	"log"
	"sendify/codecase/config"
	"sendify/codecase/foundation/web"
)

func main() {
	cfg, err := config.Load()
	if err != nil {
		log.Fatal(err.Error())
	}

	// Perform the startup and shutdown sequence.
	if err = run(cfg); err != nil {
		log.Fatal("failed to start server", err.Error())
	}

}

func run(cfg config.Config) error {
	app, err := web.NewApp(web.Config{
		User:     cfg.Database.User,
		Password: cfg.Database.Password,
		Host:     cfg.Database.Host,
		Name:     cfg.Database.Name,
	})
	if err != nil {
		return err
	}

	err = app.Serve(cfg.Port)
	if err != nil {
		return err
	}
	return nil
}
