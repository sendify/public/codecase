package api

import (
	"github.com/gin-gonic/gin"
)

// addRoutes inserts HTTP API paths and handlers into a Gin-framework engine.
// Customize application API paths here.
func (api API) Routes(engine *gin.Engine) {
	base := engine.Group("/")
	h := api.Handlers
	base.GET("shipments/:id", h.Shipments.GetShipment)
}
