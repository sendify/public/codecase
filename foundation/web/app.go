package web

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
	"sendify/codecase/api"
	"sendify/codecase/database"
)

type App struct {
	Engine *gin.Engine
	API    api.API
	DB     *gorm.DB
}

type Config struct {
	User     string
	Password string
	Host     string
	Name     string
}

func NewApp(cfg Config) (App, error) {
	db, err := database.GetConnection(database.Config{
		User:     cfg.User,
		Password: cfg.Password,
		Host:     cfg.Host,
		Name:     cfg.Name,
	})
	if err != nil {
		return App{}, err
	}
	ge := gin.Default()
	rest := api.NewAPI(db)
	rest.Routes(ge)
	return App{
		Engine: ge,
		API:    rest,
		DB:     db,
	}, nil
}

func (app *App) Serve(port string) error {
	server := http.Server{
		Addr:    port,
		Handler: app.Engine,
	}
	// Make a channel to listen for errors coming from the listener. Use a
	// buffered channel so the goroutine can exit if we don't collect this error.
	serverErrors := make(chan error, 1)

	// Start the service listening for api requests.
	go func() {
		serverErrors <- server.ListenAndServe()
	}()

	return fmt.Errorf("server error: %w", <-serverErrors)

}
