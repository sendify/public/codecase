package database

import (
	"gorm.io/gorm"
)

// Migrate migrates the database, if needed. This will be done if the Shipment model is altered,
// or if it's the first time you start the API.
func migrate(connection *gorm.DB) error {
	if err := connection.AutoMigrate(&Shipment{}); err != nil {
		return err
	}
	return nil
}
