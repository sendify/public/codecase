package main

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"os"
	"sendify/codecase/database"
)

// seeds the local database with a few shipments. only intended for use during development
// should be run with `make seed-db` to make sure that the database is up and running.
// hardcoded credentials below are the default from the docker setup
func main() {
	conn, err := gorm.Open(mysql.New(mysql.Config{
		DSN: fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4&parseTime=True", "root", "", "localhost:3306", "sendify"),
	}))
	if err != nil {
		os.Exit(1)
	}
	var shipments = []database.Shipment{
		{
			FromName:        "Sara Abdi",
			FromAddress:     "Torpagatan 2B, 330 32 Gislaved",
			FromCountryCode: "SE",
			ToName:          "Jan-Åke Persson",
			ToAddress:       "Johannesvägen 21, 438 39 Landvetter",
			ToCountryCode:   "SE",
			WeightKg:        5.2,
			Price:           100,
		},
		{

			FromName:        "Jonas Stenström",
			FromAddress:     "Herrskapsvägen 29, Karlstad 65218",
			FromCountryCode: "SE",
			ToName:          "Zhao Shan",
			ToAddress:       "9 Hill Rd Singapore 415934",
			ToCountryCode:   "SG",
			WeightKg:        13,
			Price:           750,
		},
		{

			FromName:        "Lise Livgard",
			FromAddress:     "Kullavägen 32 44441 Stenungsund",
			FromCountryCode: "SE",
			ToName:          "Jørgen Fjeld",
			ToAddress:       "Postvegen 5, 5232 Bergen",
			ToCountryCode:   "NO",
			WeightKg:        140.2,
			Price:           2000,
		},
		{

			FromName:        "Anders Liselius",
			FromAddress:     "Nygatan 32 93133 Skellefteå",
			FromCountryCode: "SE",
			ToName:          "Julia Heydecke",
			ToAddress:       "70C Christofstraße Walzbachtal 75045",
			ToCountryCode:   "DE",
			WeightKg:        3.20,
			Price:           150,
		},
	}
	conn.Create(shipments)
}
