package shipments

// IDParams captures the id param from the URI.
type IDParams struct {
	ID int64 `uri:"id" validate:"required"`
}
