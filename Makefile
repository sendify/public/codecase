
up:
	docker-compose up -d

down:
	docker-compose down

logs:
	docker-compose logs -f

logs-api:
	docker-compose logs -f api

start-db:
	docker-compose up -d db && \
	docker-compose exec -T db mysqladmin ping --wait=30 --silent

seed-db: start-db
	@go run ./database/seed

run-integration-tests:
		docker volume prune -f && \
		docker-compose -f docker-compose.test.yml up --build --abort-on-container-exit 

run-unit-tests:
	@go test -short ./...

lint:
	golangci-lint run
