# Sendify Code Case

Welcome to Sendify's Code Case API.

## Setup

The API requires

- Go 1.19
- Docker
- Docker Compose
- golangci-lint

## Starting the server

The server can be started by running `make up`. This also starts the database. The server will be available on `localhost:1234`, unless otherwise specified.

Hot reload is enabled, the server will restart when you save a `.go` file

## Logs

The server starts as a background process, you can access the logs by either

- `make logs` outputs logs for both the database and the api.
- `make logs-api` only outputs logs for the api.

## Seeding the database

You can add initial data to the database by running `make seed-db`.

## Lint

Run `make lint` to lint your code. requires `golangci-lint` to be installed on your machine

## Tests

The app has a few unit tests, testing the shipment handler. You can run them using `make run-unit-tests`.

There is a happy path integration test available for the GET `shipments/:id` endpoint. You can run it using `make run-integration-tests`.

## Usage

The API has a single endpoint.

### GET `shipments/:id`

Returns a single shipment based on its ID. Response sample:

```
Retrieving a valid shipment

Status: 200
Content type: application/json
Data:
{
  "id": 1,
  "fromAddress": "Torpagatan 2B, 330 32 Gislaved",
  "fromCountryCode": "SE",
  "fromName": "Sara Abdi",
  "toAddress": "Johannesvägen 21, 438 39 Landvetter",
  "toCountryCode": "SE",
  "toName": "Jan-Åke Persson",
  "price": 100,
  "weightKg": 5.2
}

```

```
Retrieving a non-existing shipment

Status: 404
```

```
Trying to retrieve a shipment without sending an ID

Status: 400
```
