package database

import (
	"gorm.io/gorm"
	"time"
)

type Shipment struct {
	ID              uint           `json:"id" gorm:"primarykey"`
	CreatedAt       time.Time      `json:"-"`
	UpdatedAt       time.Time      `json:"-"`
	DeletedAt       gorm.DeletedAt `json:"-" gorm:"index"`
	FromAddress     string         `json:"fromAddress" gorm:"type:varchar(100)"` //TODO json should be camelCase
	FromCountryCode string         `json:"fromCountryCode" gorm:"type:char(2)"`
	FromName        string         `json:"fromName" gorm:"type:varchar(30)"`
	ToAddress       string         `json:"toAddress" gorm:"type:varchar(100)"`
	ToCountryCode   string         `json:"toCountryCode" gorm:"type:char(2)"`
	ToName          string         `json:"toName" gorm:"type:varchar(30)"`
	Price           float64        `json:"price"`
	WeightKg        float64        `json:"weightKg"`
}
